# NGSolve User Meeting 2017
# Fluid structure interaction (Michael Neunteufel)

from ngsolve import *
from ngsolve.internal import visoptions
from math import pi
from netgen.geom2d import SplineGeometry


def IdentityCF(dim):
    return CoefficientFunction( tuple( [1 if i==j else 0 for i in range(dim) for j in range(dim)]), dims=(dim,dim) )

def Det(mat):
    return mat[0,0]*mat[1,1]-mat[0,1]*mat[1,0]

def Trace(mat):
    return sum( [mat[i,i] for i in range(mat.dims[0]) ])

def Det(mat):
    return mat[0,0]*mat[1,1]-mat[0,1]*mat[1,0]

def GenerateMesh(order, maxh=0.12, L=2.5, H=0.41):   
    geom = SplineGeometry()
    pnts = [ (0,0), (L,0), (L,H), (0,H), (0.2,0.15), (0.240824829046386,0.15), (0.248989794855664,0.19), (0.25,0.2), (0.248989794855664,0.21), (0.240824829046386,0.25), (0.2,0.25), (0.15,0.25), (0.15,0.2), (0.15,0.15), (0.6,0.19), (0.6,0.21), (0.55,0.19), (0.56,0.15), (0.6,0.15), (0.65,0.15), (0.65,0.2),(0.65,0.25), (0.6,0.25), (0.56,0.25), (0.55,0.21), (0.65,0.25),(0.65,0.15) ]
    pind = [ geom.AppendPoint(*pnt) for pnt in pnts ]

    geom.Append(['line',pind[0],pind[1]],leftdomain=1,rightdomain=0,bc=3)
    geom.Append(['line',pind[1],pind[2]],leftdomain=1,rightdomain=0,bc=2)
    geom.Append(['line',pind[2],pind[3]],leftdomain=1,rightdomain=0,bc=3)
    geom.Append(['line',pind[3],pind[0]],leftdomain=1,rightdomain=0,bc=1)

    geom.Append(['spline3',pind[4],pind[5],pind[6]],leftdomain=0,rightdomain=1,bc=4)
    geom.Append(['spline3',pind[6],pind[7],pind[8]],leftdomain=0,rightdomain=2,bc=5)
    geom.Append(['spline3',pind[8],pind[9],pind[10]],leftdomain=0,rightdomain=1,bc=4)
    geom.Append(['spline3',pind[10],pind[11],pind[12]],leftdomain=0,rightdomain=1,bc=4)
    geom.Append(['spline3',pind[12],pind[13],pind[4]],leftdomain=0,rightdomain=1,bc=4)

    geom.Append(['line',pind[6],pind[14]],leftdomain=2,rightdomain=1,bc=6)
    geom.Append(['line',pind[14],pind[15]],leftdomain=2,rightdomain=1,bc=6)
    geom.Append(['line',pind[15],pind[8]],leftdomain=2,rightdomain=1,bc=6)


    geom.SetMaterial(1, "fluid")
    geom.SetMaterial(2, "solid")
    
    mesh = Mesh(geom.GenerateMesh(maxh=maxh))
    mesh.Curve(order)
    
    return mesh

def SetCoeff(selection):
    (rhos, ls, mus, rhof, nuf, U) = (0,0,0,0,0,0)
    if(selection == 1):
        rhos = 1e3
        nus = 0.4
        mus = 0.5*1e6
        rhof = 1e3
        nuf = 1e-3
        U = 0.2
    elif(selection == 2):
        rhos = 1e4
        nus = 0.4
        mus = 0.5*1e6
        rhof = 1e3
        nuf = 1e-3
        U = 1
    elif(selection == 3):
        rhos = 1e3
        nus = 0.4
        mus = 2*1e6
        rhof = 1e3
        nuf = 1e-3
        U = 2
    else:
        print("Wrong input! Only supporting FSI 1,2 and 3!")
        rhos = 1
        nus = 1
        mus = 1
        rhof = 1
        nuf = 1e-3
        U = 2
    ls = 2*mus*nus/(1-2*nus)

    return (rhos, ls, mus, rhof, nuf, U)


tau = 0.001
order = 3

#SetNumThreads(12)
#ngsglobals.msg_level = 1

mesh = GenerateMesh(order)

(rhos, ls, mus, rhof, nuf, U) = SetCoeff(2)

#Taylor Hood
Vx = H1 ( mesh, order = order, dirichlet = [1,3,4,5] )
Vy = H1 ( mesh, order = order, dirichlet = [1,3,4,5] )
Q  = H1 ( mesh, order = order - 1, definedon = "fluid" )
Dx = H1 ( mesh, order = order, dirichlet = [1,2,3,4,5] )
Dy = H1 ( mesh, order = order, dirichlet = [1,2,3,4,5] )
X = FESpace( [Vx, Vy, Q, Dx, Dy] )

ux,uy,p,dx,dy = X.TrialFunction()
vx,vy,q,wx,wy = X.TestFunction()

u = CoefficientFunction( (ux, uy) )
v = CoefficientFunction( (vx, vy) )
d = CoefficientFunction( (dx, dy) )
w = CoefficientFunction( (wx, wy) )

gradu = CoefficientFunction( (ux.Deriv(), uy.Deriv()), dims = [2,2] )
gradv = CoefficientFunction( (vx.Deriv(), vy.Deriv()), dims = [2,2] )
gradd = CoefficientFunction( (dx.Deriv(), dy.Deriv()), dims = [2,2] )
gradw = CoefficientFunction( (wx.Deriv(), wy.Deriv()), dims = [2,2] )

gfu    = GridFunction(X)
gfuold = GridFunction(X)
velocity = CoefficientFunction( gfu.components[0:2] )
pressure = CoefficientFunction( gfu.components[2] )
deformation = CoefficientFunction( gfu.components[3:5] )
velocityold = CoefficientFunction( gfuold.components[0:2] )
deformationold = CoefficientFunction( gfuold.components[3:5] )

graduold = CoefficientFunction( (gfuold.components[0].Deriv(), gfuold.components[1].Deriv()), dims = [2,2] )
graddold = CoefficientFunction( (gfuold.components[3].Deriv(), gfuold.components[4].Deriv()), dims = [2,2] )


Id = IdentityCF( mesh.dim )


F = gradd + Id
C = F.trans * F
E = 0.5*(C - Id)
J = Det(F)
Finv = 1/J*CoefficientFunction( (F[1,1],-F[0,1],-F[1,0],F[0,0]), dims = [2,2] )

compileflag = False#True

#Define big BilinearForm for Newton
a = BilinearForm( X, symmetric = False )
# M du/dt
a += SymbolicBFI( (rhof*InnerProduct(J*(u-velocityold), v)).Compile(compileflag), definedon = mesh.Materials("fluid") )
# Laplace
a += SymbolicBFI( (rhof*nuf*tau*InnerProduct(J*gradu*Finv, gradv*Finv)).Compile(compileflag), definedon = mesh.Materials("fluid") )
# Convection
a += SymbolicBFI( (rhof*tau*InnerProduct(J*(gradu*Finv)*u, v)).Compile(compileflag), definedon = mesh.Materials("fluid") )
# mesh velocity
a += SymbolicBFI( (-rhof*InnerProduct((J*gradu*Finv)*(d-deformationold), v)).Compile(compileflag), definedon = mesh.Materials("fluid") )
# Pressure/Constraint
a += SymbolicBFI( (-tau*J*(Trace(gradv*Finv)*p+Trace(gradu*Finv)*q)).Compile(compileflag), definedon = mesh.Materials("fluid") )

# M du/dt
a += SymbolicBFI( (rhos*InnerProduct(u-velocityold, v)).Compile(compileflag), definedon = mesh.Materials("solid") )
# Material law
a += SymbolicBFI( (tau*InnerProduct(F*(2*mus*E+ls*Trace(E)*Id), gradv)).Compile(compileflag), definedon = mesh.Materials("solid") )
#dd/dt = u
a += SymbolicBFI( (InnerProduct(u+velocityold-2.0/tau*(d-deformationold), w)).Compile(compileflag), definedon = mesh.Materials("solid") )

#deformation extension
factor = 1e-8
a += SymbolicBFI( (tau*factor*InnerProduct(1/J*gradd,gradw)).Compile(compileflag), definedon = mesh.Materials("fluid") )

#Drawing stuff
Draw(velocity, mesh, "velocity")
Draw(pressure, mesh, "pressure")
Draw(deformation, mesh, "deformation")
visoptions.scalfunction = "velocity:0"
visoptions.vecfunction = "deformation"
visoptions.deformation = 1


#inflow velocity
def Force(t):
    if t < 0:
        return 0
    elif t < 2:
        return (1-cos(pi/2.0*t))/2.0
    else:
        return 1


tmp = GridFunction(X)

w = gfu.vec.CreateVector()

uinflow = 4*U*1.5*y*(0.41-y)/(0.41*0.41)

Redraw()
t = 0
tend = 15

r = GridFunction(X)


with TaskManager():
    while t < tend-tau/2.0:
        t += tau
        print("t = ", t)

        #update inflow
        if t < 2 + tau/2.0:
            tmp.components[0].Set( (Force(t)-Force(t-tau))*uinflow*CoefficientFunction([1,0,0,0,0,0]), BND )
            gfu.components[0].vec.data += tmp.components[0].vec

        #Newton
        for it in range(10):
            a.Apply(gfu.vec, r.vec)
            a.AssembleLinearization(gfu.vec)
            inv = a.mat.Inverse(X.FreeDofs())
            # inv = a.mat.Inverse(X.FreeDofs(), inverse="pardiso")
            
            w.data = inv*r.vec
            
            err = InnerProduct(w,r.vec)
            print("err^2 = ", err)
            if abs(err) < 1e-25: break    
            
            gfu.vec.data -= w

        Redraw()

        gfuold.vec.data = gfu.vec
